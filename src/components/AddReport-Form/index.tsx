/* eslint-disable jsx-a11y/anchor-is-valid */
// eslint-disable-next-line no-use-before-define
import React from "react";
import AddReportFormWrapper from "./styled";

export const AddReportForm = () => {
  return (
    <AddReportFormWrapper className="text">
      <div>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb aha-breadcrumb">
            <li className="breadcrumb-item">
              <a href="#">Projects</a>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <a href="#">ShopCPR</a>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              <a href="#">Add Report</a>
            </li>
          </ol>
        </div>
        <div className="title">ShopCPR Report</div>
        <form className="form">
          <div className="form-entry scopes">
            <div className=" d-flex  form-group row required  col-12 justify-content-between pl-0 pr-0 mr-0 ml-0 asdf">
              <label
                htmlFor="scope"
                className="col-2 col-form-label scope pl-0 pr-0 pt-0"
              >
                Scope{" "}
              </label>

              <input
                type="text"
                className="form-control col-md-7"
                id="scope"
                aria-label="enter scope"
              />

              <div className="toggle-buttons d-flex col-2 align-items-top pl-0 pr-0">
                <button
                  className="toggle-buttons-green col-6 text-center align-middle bg-success text-white pt-0 pb-0 pr-0 pl-0 "
                  aria-label="green toggle"
                  type="button"
                  id="scope"
                >
                  <p>Green</p>
                </button>
                <button
                  className="toggle-buttons-red col-6 text-center align-middle pt-0 pb-0 pr-0 pl-0 "
                  aria-label="red toggle"
                  type="button"
                  id="scope"
                >
                  <p>Red</p>
                </button>
              </div>
            </div>
            <div className="form-group row d-flex justify-content-center col-sm-12 pl-0 pr-0 mb-0 ml-0 mr-0">
              <textarea
                className="form-control col-md-7 textarea"
                id="scope"
                placeholder="Add Comment"
              />
            </div>
          </div>
          <div className="form-entry">
            <div className=" d-flex  form-group row required  col-sm-12 justify-content-between pl-0 pr-0 mr-0 ml-0 asdf">
              <label
                htmlFor="quality"
                className="col-2 col-form-label quality pl-0 pr-0 pt-0"
              >
                Quality{" "}
              </label>

              <input
                type="text"
                className="form-control col-md-7"
                id="quality"
                aria-label="enter quality"
              />

              <div className="toggle-buttons d-flex col-2 align-items-top pl-0 pr-0">
                <button
                  className="toggle-buttons-green col-6 text-center align-middle bg-success text-white pt-0 pb-0 pr-0 pl-0 "
                  aria-label="green toggle"
                  type="button"
                  id="quality"
                >
                  <p>Green</p>
                </button>
                <button
                  className="toggle-buttons-red col-6 text-center align-middle pt-0 pb-0 pr-0 pl-0 "
                  aria-label="red toggle"
                  type="button"
                  id="quality"
                >
                  <p>Red</p>
                </button>
              </div>
            </div>
            <div className="form-group row d-flex justify-content-center col-sm-12 pl-0 pr-0 mb-0 ml-0 mr-0">
              <textarea
                className="form-control col-md-7 textarea"
                id="quality"
                placeholder="Add Comment"
              />
            </div>
          </div>
          <div className="form-entry schedules">
            <div className=" d-flex  form-group row required  col-sm-12 justify-content-between pl-0 pr-0 mr-0 ml-0 asdf">
              <label
                htmlFor="schedule"
                className="col-2 col-form-label schedule pl-0 pr-0 pt-0"
              >
                Schedule{" "}
              </label>

              <input
                type="text"
                className="form-control col-md-7"
                id="schedule"
                aria-label="enter schedule"
              />

              <div className="toggle-buttons d-flex col-2 align-items-top pl-0 pr-0">
                <button
                  className="toggle-buttons-green col-6 text-center align-middle bg-success text-white pt-0 pb-0 pr-0 pl-0 "
                  aria-label="green toggle"
                  type="button"
                  id="schedule"
                >
                  <p>Green</p>
                </button>
                <button
                  className="toggle-buttons-red col-6 text-center align-middle pt-0 pb-0 pr-0 pl-0 "
                  aria-label="red toggle"
                  type="button"
                  id="schedule"
                >
                  <p>Red</p>
                </button>
              </div>
            </div>
            <div className="form-group row d-flex justify-content-center col-sm-12 pl-0 pr-0 mb-0 ml-0 mr-0">
              <textarea
                className="form-control col-md-7 textarea"
                id="schedule"
                placeholder="Add Comment"
              />
            </div>
          </div>
          <div className="form-entry">
            <div className=" d-flex  form-group row required  col-sm-12 justify-content-between pl-0 pr-0 mr-0 ml-0 asdf">
              <label
                htmlFor="budget"
                className="col-2 col-form-label budget pl-0 pr-0 pt-0"
              >
                Budget{" "}
              </label>

              <input
                type="text"
                className="form-control col-md-7"
                id="budget"
                aria-label="enter budget"
              />

              <div className="toggle-buttons d-flex col-2 align-items-top pl-0 pr-0">
                <button
                  className="toggle-buttons-green col-6 text-center align-middle bg-success text-white pt-0 pb-0 pr-0 pl-0 "
                  aria-label="green toggle"
                  type="button"
                  id="budget"
                >
                  <p>Green</p>
                </button>
                <button
                  className="toggle-buttons-red col-6 text-center align-middle pt-0 pb-0 pr-0 pl-0 "
                  aria-label="red toggle"
                  type="button"
                  id="budget"
                >
                  <p>Red</p>
                </button>
              </div>
            </div>
            <div className="form-group row d-flex justify-content-center col-sm-12 pl-0 pr-0 mb-0 ml-0 mr-0">
              <textarea
                className="form-control col-md-7 textarea"
                id="budget"
                placeholder="Add Comment"
              />
            </div>
          </div>
          <div className="form-entry overall-healths">
            <div className=" d-flex  form-group row required  col-sm-12 justify-content-between pl-0 pr-0 mr-0 ml-0 asdf pb-0 mb-0">
              <label
                htmlFor="overall-health"
                className="col-2 col-form-label overall-health pl-0 pr-0 pt-0 pb-0"
              >
                Overall Health (%){" "}
              </label>

              <input
                type="text"
                className="form-control col-md-7"
                id="overall-health"
                aria-label="enter overall-health"
              />

              <div className="toggle-buttons d-flex col-2 align-items-top pl-0 pr-0">
                <button
                  className="toggle-buttons-green col-6 text-center align-middle bg-success text-white pt-0 pb-0 pr-0 pl-0 "
                  aria-label="green toggle"
                  type="button"
                  id="overall-health"
                >
                  <p>Green</p>
                </button>
                <button
                  className="toggle-buttons-red col-6 text-center align-middle pt-0 pb-0 pr-0 pl-0 "
                  aria-label="red toggle"
                  type="button"
                  id="overall-health"
                >
                  <p>Red</p>
                </button>
              </div>
            </div>
            <div className="form-group row d-flex justify-content-center col-sm-12 pl-0 pr-0 mb-0 ml-0 mr-0 oh">
              <textarea
                className="form-control col-md-7 textarea"
                id="overall-health"
                placeholder="Add Comment"
              />
            </div>
          </div>
          <div className="lines" />
          <div className="buttons d-flex justify-content-end flex-wrap">
            <button
              className="btn a btn-round btn-secondary text-center p-0"
              type="button"
            >
              Cancel
            </button>
            <button
              className="btn b btn-round btn-primary text-center p-0"
              type="submit"
            >
              Save
            </button>
          </div>
        </form>
      </div>
    </AddReportFormWrapper>
  );
};
export default AddReportForm;
