// eslint-disable-next-line no-use-before-define
import React from "react";
import CardsWrapper from "./styled";

export const DashBoardCards = () => {
  return (
    <CardsWrapper>
      <div
        className="first-container d-flex flex-row justify-content-between flex-wrap"
        aria-label="Overview of Projects"
      >
        <div className="col-md-4 p-0">
          <div className="cardboard_a d-flex flex-row">
            <img src="images/graph@2x.png" alt="graph" className="image-a" />

            <div className="text_a d-flex flex-column justify-content-between ">
              <p>Total Projects</p>
              <p className="values">51</p>
            </div>
          </div>
        </div>
        <div className="col-md-4 p-0">
          <div className="cardboard_b d-flex flex-row">
            <img src="images/increase@2x.png" alt="graph" className="image-b" />

            <div className="text_b d-flex flex-column justify-content-between ">
              <p>Active Projects</p>
              <p className="values">30</p>
            </div>
          </div>
        </div>
        <div className="col-md-4 p-0">
          <div className="cardboard_c d-flex flex-row">
            <img src="images/process@2x.png" alt="graph" className="image-c" />

            <div className="text_c d-flex flex-column justify-content-between">
              <p>Upcoming Projects</p>
              <p className="values">5</p>
            </div>
          </div>
        </div>
      </div>
    </CardsWrapper>
  );
};
export default DashBoardCards;
