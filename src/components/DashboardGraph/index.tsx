// eslint-disable-next-line no-use-before-define
import React from "react";
import GraphWrapper from "./styled";

export const DashboardGraph = () => {
  return (
    <GraphWrapper>
      <div className="second-container d-flex">
        <div className="graphs col-lg-6 p-0">
          <div className="graph-heading d-flex justify-content-between ">
            <div className="graph-topic ">Statistics</div>
            <div className="graph-duration">
              <div className="navbar aui-profile d-inline-block">
                <button
                  aria-label="user-profile"
                  className="btn btn-text aui-dropdown aha-icon-arrow-down dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  type="button"
                >
                  <i className="pr-2" />
                  Last 6 months
                </button>
                <ul className="aui-topright-arrow dropdown-menu p-lg-0 header-dropdown-menu">
                  <li>
                    <a className="dropdown-item py-2" href="/">
                      Profile Settings
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item py-2" href="/">
                      Sign Out
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div>
            <div className="graph-project">
              <img
                src="images/dashboard-image.png"
                alt="project-information-graph"
                className="graph-imagess"
              />
            </div>
          </div>
        </div>
        <div className="Reports p-0">
          <div className=" report-title bg-dark  d-flex align-items-center justify-content-between ">
            <div className="Heading">Project Updates</div>
            <img
              src="images/group 6130.png"
              alt="refresh-icon"
              className="image-a"
            />
          </div>

          <div className=" table-body">
            <div className=" report-recent">
              <div className="datas d-flex justify-content-between">
                <div>shopCPR</div>
                <div className="scope">Scope</div>
              </div>
              <div className="report-text">
                Contrary to popular belief, Lorem Ipsum is not simply random
                text. It has roots in a piece of classical Latin literature from
                45
              </div>
              <div className="date d-flex justify-content-end">9/12/2021</div>
            </div>
            <div className=" report-recent">
              <div className="datas d-flex justify-content-between">
                <div>shopCPR</div>
                <div className="scope">Scope</div>
              </div>
              <div className="report-text">
                Contrary to popular belief, Lorem Ipsum is not simply random
                text. It has roots in a piece of classical Latin literature from
                45
              </div>
              <div className="date d-flex justify-content-end">9/12/2021</div>
            </div>

            <div className=" report-recent">
              <div className="datas d-flex justify-content-between ">
                <div>shopCPR</div>
                <div className="scope">Scope</div>
              </div>
              <div className="report-text">
                Contrary to popular belief, Lorem Ipsum is not simply random
                text. It has roots in a piece of classical Latin literature from
                45
              </div>
              <div className="date d-flex justify-content-end">9/12/2021</div>
            </div>
          </div>
        </div>
      </div>
    </GraphWrapper>
  );
};
export default DashboardGraph;
