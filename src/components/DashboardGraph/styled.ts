import styled from "styled-components";

const GraphWrapper = styled.div`
  .second-container {
    padding-top: 60px;
    flex-direction: column;
    .graph-topic {
      font-size: 24px;
      line-height: 1.1;
    }
    .graph-duration {
      .btn {
        padding-top: 3px;
        padding-bottom: 3px;
        font-size: 14px;
        color: #4d4f5c;
        border: 1px solid #d7dae2;
        max-width: 130px;
      }
      .navbar.aui-profile {
        padding: 0px;
        height: 32px;
        top: 0 !important;
        bottom: 0 !important;
        color: #4d4f5c !important;
      }
      .aui-dropdown {
        height: 32px;
      }
      .aui-profile,
      .aui-dropdown::before {
        top: 0.75rem;
      }
      .header-dropdown-menu {
        min-width: 7rem;
        font-size: 0.8rem;
      }
      .dropdown-item {
        padding: 0.5rem;
      }
    }
    .graph-project {
      .graph-imagess {
        height: 100%;
        width: 100%;
        object-fit: contain;
        height: 370px;
      }
    }
    .Reports {
      margin-left: 0px;
      .report-title {
        height: 57px;
        color: #ffffff;
        padding-left: 18px;
        padding-right: 18px;
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
      }
      .table-body {
        padding-left: 18px;
        padding-right: 18px;
        border-bottom: 1px solid #e3e3e3;
        border-left: 1px solid #e3e3e3;
        border-right: 1px solid #e3e3e3;
        border-bottom-left-radius: 8px;
        border-bottom-right-radius: 8px;

        .report-recent {
          border-bottom: 1px solid #e3e3e3;

          .datas {
            font-size: 14px;
            font-weight: 600;
            padding-top: 12px;
            padding-bottom: 12px;
            .scope {
              color: #0d8200;
            }
          }
          .report-text {
            font-size: 14px;
          }
          .date {
            font-size: 12px;
            font-weight: 600;
            padding-top: 4px;
            padding-bottom: 13.5px;
          }
        }
      }
    }
    @media (min-width: 992px) {
      flex-direction: row;
      .Reports {
        margin-left: 27px;
      }
    }
  }
`;
export default GraphWrapper;
