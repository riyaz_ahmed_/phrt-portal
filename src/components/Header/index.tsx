/* eslint-disable jsx-a11y/label-has-associated-control */
import MyAccounts from "components/MyAccount/MyAccount";
import SearchBars from "components/SearchBar/SearchBar";
import React from "react";
import TopNavWrapper from "./styled";

export const Header = () => {
  return (
    <TopNavWrapper>
      <header className="flex-basis-prop">
        <SearchBars />
        <MyAccounts />
      </header>
    </TopNavWrapper>
  );
};
export default Header;
