import styled from "styled-components";

export const Table = styled.div`
  width: 100%;
  height: auto;
  & .table {
    width: 100%;
    & tbody {
      & tr {
        & td {
          border: 1px solid #e3e3e3;
          text-align: left;
          vertical-align: middle;
        }
        & .title {
          color: #c10e21;
          font-weight: 600;
          text-decoration: underline;
          & .justify-align {
            word-break: keep-all;
          }
        }
        & div {
          .justify-align {
            word-break: keep-all;
            & .Red {
              color: #eb0f25;
              font-weight: normal;
            }
            & .Green {
              color: #0d8200;
              font-weight: normal;
            }
          }
          & .Red {
            color: #eb0f25;
            font-weight: normal;
          }
          & .Green {
            color: #0d8200;
            font-weight: normal;
          }
          .Change {
            display: flex;
            flex-direction: row;
            word-break: keep-all;
          }
        }
      }
    }
  }
`;
export default Table;
