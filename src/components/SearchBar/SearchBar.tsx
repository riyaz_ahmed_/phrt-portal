/* eslint-disable prettier/prettier */
import React from "react";
import { SearchBarr } from "./styled";

function SearchBars() {
    return (
        <SearchBarr className="search-container">
            <form action="/action_page.php">
                <input
                    id="searchbar"
                    type="search"
                    placeholder="Search"
                    name="search"
                    aria-label="Search Bar"
                />
            </form>
        </SearchBarr>
    );
}

export default SearchBars;
