import styled from "styled-components";

export const ProjectListWrapper = styled.div`
  min-height: 100vh;
  #maincontent {
    margin-inline: 20px;
    @media (min-width: 992px) {
      margin: 18px 21px 18px 21px;
      height: 100%;
      flex-grow: 1;
    }
    & .content-container {
      display: flex;
      flex-direction: column;
      @media (min-width: 992px) {
        flex-direction: row;
      }
      & .left-container {
        padding: 0px;
        @media (min-width: 991px) {
          padding: 19px 10px 19px 31px;
        }
        & div {
          & .heading-bar {
            flex-direction: column;
            padding-bottom: 20px;
            @media (min-width: 576px) {
              flex-direction: row;
            }
            & .font-styled {
              font: normal normal 600 22px/49px Montserrat;
              letter-spacing: 0px;
              color: #000000;
              opacity: 1;
            }
            & button.btn-height {
              /* width: 50%; */
              height: auto;
              /* width: 60%; */
              max-height: 40px;
              padding: 8px 30px 8px 30px;
              @media (min-width: 575px) {
                /* width: 50%; */
                /* max-width: 160px; */
              }
            }
          }
          & .status-bar {
            flex-direction: column;
            @media (min-width: 576px) {
              flex-direction: row;
            }
            & .flex-prop-media {
              div {
                .Red {
                  color: #eb0f25;
                  font-weight: normal;
                }
                .Green {
                  color: #0d8200;
                  font-weight: normal;
                }
              }
            }
          }
        }
      }
    }
  }
`;
export const AnotherWraper = styled.div``;
