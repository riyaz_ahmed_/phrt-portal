import React from "react";
import { Switch } from "react-router-dom";
import LoginPage from "pages/Loginpage/Loginpage";
import Dashboard from "pages/Dashboard/DashboardPage";
import ProjectList from "pages/Projects/ProjectPage";
import Project from "pages/ProjectName/ProjectName";
import AddProject from "pages/Add_Project/Addproject";
import AddReport from "pages/Add_Report/Addreportpage";
import Route from "./Route";

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={LoginPage as any} hideHeader />
      <Route path="/dashboard" component={Dashboard as any} hideHeader />
      <Route
        path="/projectlist"
        exact
        component={ProjectList as any}
        hideHeader
      />
      <Route
        path="/projectlist/projectname"
        component={Project as any}
        hideHeader
      />
      <Route
        path="/projectlist/addproject"
        component={AddProject as any}
        hideHeader
      />
      <Route
        path="/projectlist/addreport"
        component={AddReport as any}
        hideHeader
      />
    </Switch>
  );
}
